var express = require('express');
var randomstring = require("randomstring");
var router = express.Router();

/* GET home page. */

// 127.0.0.1:3000


router.get('/', function(req, res, next) {
  res.render('index');
});


router.get('/reurl', function(req, res, next) { // 短網址request API
  var new_url = req.query.new; // 擷取短網址參數
  var date_now = new Date;
  
  if(new_url){
    var db = req.con;
    db.query('SELECT `target_url`,`time` FROM `transform` WHERE `new_url`=? ',[new_url],function(err,rows){
      // 進行參數查詢
      if(err){
        res.send('this short url not found') // 短網址錯誤
      }else{
        if(date_now>rows[0].time){ // 檢查短網址是否過期
          res.send("url expired");
        }else{
          res.redirect(rows[0].target_url); // 跳轉至目標網址
        }
      }
    })
  }else{
    res.send("this short url not found")
  }
});


router.post('/reurl', function(req, res, next) { // 上傳短網址API
  var target = req.body.target;
  var short = req.body.short;
  var date = new Date;
  date.setDate(date.getDate()+1); // 到期時間往後推一天
  if(target){   
    var db = req.con;
    if(!short){
      short = randomstring.generate(10);  // 若無指定短網址，隨機產生
    }
      db.query('INSERT INTO `transform` (`target_url`, `new_url`, `time`) VALUES (?,?,?);',[target,short,date],function(err){
        // 寫入資料庫
        if(err){
          res.send("short url is existed") // 指定短網址已經使用過
        }else{
          short = "127.0.0.1:3000/reurl?new="+short; // 回傳短網址
          res.send(short);
        }
      })
  }else{
    res.send("target url cant be empty") // 目標網址必填
  }
});




module.exports = router;
